
// Playing Cards
// Wyatt Baughman

// Forked and modified by Austin Lennert

#include <iostream>
#include <conio.h>

using namespace std;


enum Suit
{
	Hearts,
	Spades,
	Diamonds,
	Clubs
};

enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Suit Suit = Clubs;
	Rank Rank = Ace;
};

void printCard(Card card);

Card highCard(const Card& card1, const Card& card2);


int main()
{
	Card c1;
	c1.Rank = Seven;

	Card c2;
	c2.Rank = King;
	
	cout << "The higher card is ";
	printCard(highCard(c1, c2));


	cout << endl;


	for (int suit = Hearts; suit <= Clubs; suit++) {
		for (int rank = Two; rank <= Ace; rank++) {
			Card card;
			card.Suit = static_cast<Suit>(suit);  // Convert int to respective enum value
			card.Rank = static_cast<Rank>(rank);  // https://stackoverflow.com/a/261986
			printCard(card);
		}
	}
	

	(void)_getch();
	return 0;
}


/**
Prints the card in the format 'The [RANK] of [SUIT]'
@param card - The card to print
*/
void printCard(Card card) {
	cout << "The ";

	switch (card.Rank) {
	case Two:
		cout << "Two";
		break;
	case Three:
		cout << "Three";
		break;
	case Four:
		cout << "Four";
		break;
	case Five:
		cout << "Five";
		break;
	case Six:
		cout << "Six";
		break;
	case Seven:
		cout << "Seven";
		break;
	case Eight:
		cout << "Eight";
		break;
	case Nine:
		cout << "Nine";
		break;
	case Ten:
		cout << "Ten";
		break;
	case Jack:
		cout << "Jack";
		break;
	case Queen:
		cout << "Queen";
		break;
	case King:
		cout << "King";
		break;
	case Ace:
		cout << "Ace";
		break;
	default:
		cout << "what rank even is this";
		break;
	}

	cout << " of ";

	switch (card.Suit) {

	case Hearts:
		cout << "Hearts";
		break;
	case Spades:
		cout << "Spades";
		break;
	case Diamonds:
		cout << "Diamonds";
		break;
	case Clubs:
		cout << "Clubs";
		break;
	default:
		cout << "this isn't even red or black how did you get this";
		break;
	}
	cout << endl;
}

/**
Determines which of the two provided cards is higher in rank
@return the higher of two cards
*/
Card highCard(const Card& card1, const Card& card2) {
	return (card1.Rank > card2.Rank ? card1 : card2);
}